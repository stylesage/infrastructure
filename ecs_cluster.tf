resource "aws_ecs_cluster" "stylesage" {
  name = var.ecs["cluster_name"]

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

data "template_file" "user_data_ecs" {
  template = file("templates/ecs/launch_instance.tpl")

  vars = {
    cluster_name = aws_ecs_cluster.stylesage.name
  }
}

resource "aws_launch_configuration" "ecs" {
  name_prefix                 = "ECS Cluster StyleSage - "
  image_id                    = var.ecs["ami"]
  instance_type               = var.ecs["instance_type"]
  iam_instance_profile        = aws_iam_instance_profile.ecs_for_ec2.name
  user_data                   = data.template_file.user_data_ecs.rendered
  associate_public_ip_address = false

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ecs" {
  name                 = aws_ecs_cluster.stylesage.name
  min_size             = var.ecs["autoscale_min"]
  max_size             = var.ecs["autoscale_max"]
  desired_capacity     = var.ecs["autoscale_desired"]
  health_check_type    = var.ecs["health_check_type"]
  launch_configuration = aws_launch_configuration.ecs.name
  vpc_zone_identifier  = module.vpc.private_subnets

  lifecycle {
    create_before_destroy = true
  }
}
