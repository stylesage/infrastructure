module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  version            = "2.24.0"
  name               = "vpc"
  cidr               = "10.0.0.0/16"
  azs                = var.azs
  private_subnets    = var.private_subnets
  public_subnets     = var.public_subnets
  enable_nat_gateway = true
  enable_vpn_gateway = true
  tags               = var.tags
}


resource "aws_default_security_group" "default" {
  vpc_id = module.vpc.vpc_id
}
