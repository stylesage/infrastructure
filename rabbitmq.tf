resource "aws_cloudwatch_log_group" "rabbitmq" {
  name              = "/ecs/rabbitmq"
  retention_in_days = 14
  tags              = var.tags
}

resource "aws_ssm_parameter" "user_password_rabbitmq" {
  name  = "/stylesage/rabbitmq/password"
  type  = "SecureString"
  value = random_password.user_password_rabbitmq.result
}

resource "random_password" "user_password_rabbitmq" {
  length  = 30
  special = true
}

data "template_file" "rabbitmq" {
  template = file("task-definitions/rabbitmq.json")

  vars = {
    awslogs_group             = aws_cloudwatch_log_group.rabbitmq.name
    hostPort                  = var.rabbitmq["port"]
    containerPort             = var.rabbitmq["port"]
    manageHostPort            = var.rabbitmq["manage_port"]
    manageContainerPort       = var.rabbitmq["manage_port"]
    rabbitmq_default_user     = var.rabbitmq["user_rabbitmq"]
    rabbitmq_default_pass_arn = aws_ssm_parameter.user_password_rabbitmq.arn
    repository_url            = var.rabbitmq["docker_image"]
    image_tag                 = var.rabbitmq["tag_docker_image"]
    name                      = var.rabbitmq["name"]
  }
}

resource "aws_ecs_task_definition" "rabbitmq" {
  family                   = "rabbitmq"
  requires_compatibilities = ["EC2"]
  network_mode             = "awsvpc"
  container_definitions    = data.template_file.rabbitmq.rendered
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn
}

resource "aws_ecs_service" "rabbitmq" {
  name            = var.rabbitmq["name"]
  cluster         = aws_ecs_cluster.stylesage.id
  task_definition = aws_ecs_task_definition.rabbitmq.arn
  desired_count   = 2

  ordered_placement_strategy {
    type  = "spread"
    field = "attribute:ecs.availability-zone"
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.rabbitmq.arn
    container_name   = var.rabbitmq["name"]
    container_port   = var.rabbitmq["port"]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.rabbitmq_manage.arn
    container_name   = var.rabbitmq["name"]
    container_port   = var.rabbitmq["manage_port"]
  }

  network_configuration {
    subnets         = module.vpc.private_subnets
    security_groups = [aws_default_security_group.default.id]
  }

  # service_registries {}

  lifecycle {
    ignore_changes = [desired_count]
  }

  depends_on = [aws_lb_target_group.rabbitmq, aws_lb_target_group.rabbitmq_manage]
}

resource "aws_appautoscaling_target" "rabbitmq" {
  max_capacity       = 2
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.stylesage.name}/${aws_ecs_service.rabbitmq.name}"
  role_arn           = aws_iam_service_linked_role.ecs_autoscaling_service.arn
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "rabbitmq_up" {
  name               = "appScalingRabbitMQScaleUpPolicy"
  service_namespace  = aws_appautoscaling_target.rabbitmq.service_namespace
  scalable_dimension = aws_appautoscaling_target.rabbitmq.scalable_dimension
  resource_id        = aws_appautoscaling_target.rabbitmq.resource_id

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = "300"
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }

  depends_on = [aws_appautoscaling_target.rabbitmq]
}

resource "aws_appautoscaling_policy" "rabbitmq_down" {
  name               = "appScalingRabbitMQScaleDownPolicy"
  service_namespace  = aws_appautoscaling_target.rabbitmq.service_namespace
  scalable_dimension = aws_appautoscaling_target.rabbitmq.scalable_dimension
  resource_id        = aws_appautoscaling_target.rabbitmq.resource_id

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = "300"
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }

  depends_on = [aws_appautoscaling_target.rabbitmq]
}

resource "aws_cloudwatch_metric_alarm" "rabbitmq_high_cpu" {
  alarm_name          = "alarmAuthCPUUtilizationHigh"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "300"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    ClusterName = aws_ecs_cluster.stylesage.name
    ServiceName = aws_ecs_service.rabbitmq.name
  }

  alarm_actions = [aws_appautoscaling_policy.rabbitmq_up.arn]
}

resource "aws_cloudwatch_metric_alarm" "rabbitmq_low_cpu" {
  alarm_name          = "alarmAuthCPUUtilizationLow"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "300"
  statistic           = "Average"
  threshold           = "60"

  dimensions = {
    ClusterName = aws_ecs_cluster.stylesage.name
    ServiceName = aws_ecs_service.rabbitmq.name
  }

  alarm_actions = [aws_appautoscaling_policy.rabbitmq_down.arn]
}


resource "aws_lb_listener" "rabbitmq" {
  load_balancer_arn = aws_lb.stylesage.arn
  port              = var.rabbitmq["port"]
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.rabbitmq.arn
  }
  depends_on = [aws_lb_target_group.rabbitmq]
}

resource "aws_lb_listener" "rabbitmq_manage" {
  load_balancer_arn = aws_lb.stylesage.arn
  port              = var.rabbitmq["manage_port"]
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.rabbitmq_manage.arn
  }
  depends_on = [aws_lb_target_group.rabbitmq_manage]
}

resource "aws_lb_target_group" "rabbitmq" {
  name        = "rabbitmq"
  port        = var.rabbitmq["port"]
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = module.vpc.vpc_id
  tags        = var.tags
  stickiness {
    enabled = false
    type    = "lb_cookie"
  }
  depends_on = [aws_lb.stylesage]
}

resource "aws_lb_target_group" "rabbitmq_manage" {
  name                 = "rabbitmq-manage"
  port                 = var.rabbitmq["manage_port"]
  protocol             = "TCP"
  target_type          = "ip"
  vpc_id               = module.vpc.vpc_id
  deregistration_delay = var.rabbitmq["deregistration_delay"]
  tags                 = var.tags
  stickiness {
    enabled = false
    type    = "lb_cookie"
  }
  depends_on = [aws_lb.stylesage]
}

