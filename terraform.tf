provider "aws" {
  region  = "eu-west-1"
  version = "~> 2.47.0"

  #   assume_role {
  #     role_arn = "arn:aws:iam::${var.account_number}:role/${var.tags["environment"]}-adnsofia-delegated-devops"
  #   }
}

provider "template" {
  version = "~> 2.1.2"
}

provider "random" {
  version = "~> 2.2.1"
}

terraform {
  required_version = "~> 0.12.20"

  backend "s3" {
    bucket               = "stylesage-terraform-state"
    key                  = "terraform.tfstate"
    workspace_key_prefix = "workspaces"
    region               = "eu-west-1"
    encrypt              = true
  }
}
