variable "rabbitmq" {
  type = map(string)
}

variable "private_subnets" {
  description = "Map from availability zone to the number that should be used for each availability zone's subnet"
  default     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}

variable "public_subnets" {
  description = "Map from availability zone to the number that should be used for each availability zone's subnet"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "azs" {
  default = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}


variable "tags" {
  type = map(string)
}

variable "ecs" {
  type = map(string)
}
