rabbitmq = {
  port                 = 5672
  manage_port          = 15672
  deregistration_delay = 30
  user_rabbitmq        = "admin"
  name                 = "rabbitmq"
  dns                  = "rabbitmq"
  docker_image         = "rabbitmq"
  tag_docker_image     = "3.8.2-management-alpine"
}

ecs = {
  ami               = "ami-01de9443606bda731"
  instance_type     = "t2.micro"
  cluster_name      = "stylesage"
  root_size         = 20
  autoscale_min     = 1
  autoscale_max     = 2
  autoscale_desired = 1
  health_check_type = "ELB"
}
