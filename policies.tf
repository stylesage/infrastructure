data "aws_iam_policy_document" "ecs_tasks_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "ecs_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "aws_task_execution_role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}



resource "aws_iam_role" "ecs_execution_role" {
  name               = "ECSTaskExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.ecs_tasks_assume_role.json
}

resource "aws_iam_role_policy_attachment" "attach_decrypt_policy" {
  role       = aws_iam_role.ecs_execution_role.name
  policy_arn = aws_iam_policy.decrypt_secrets_task.arn
}

resource "aws_iam_policy" "decrypt_secrets_task" {
  policy = data.aws_iam_policy_document.decrypt_secrets_task.json
}

data "aws_iam_policy_document" "decrypt_secrets_task" {
  statement {
    actions   = ["ssm:GetParameters"]
    resources = [aws_ssm_parameter.user_password_rabbitmq.arn]
  }
}
resource "aws_iam_role_policy_attachment" "attach_execution_role" {
  role       = aws_iam_role.ecs_execution_role.name
  policy_arn = data.aws_iam_policy.aws_task_execution_role.arn
}

resource "aws_iam_instance_profile" "ecs_for_ec2" {
  name = "AmazonECSforEC2Profile"
  role = aws_iam_role.ecs_for_ec2_role.name
}

resource "aws_iam_role" "ecs_for_ec2_role" {
  name               = "AmazonECSforEC2Role"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
}

resource "aws_iam_role_policy_attachment" "attach_ecs_for_ec2_role" {
  role       = aws_iam_role.ecs_for_ec2_role.name
  policy_arn = data.aws_iam_policy.aws_ecs_for_ec2_role.arn
}

data "aws_iam_policy" "aws_ecs_for_ec2_role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_service_linked_role" "ecs_autoscaling_service" {
  aws_service_name = "ecs.application-autoscaling.amazonaws.com"
}

resource "aws_iam_service_linked_role" "ecs_service" {
  aws_service_name = "ecs.amazonaws.com"
}

resource "aws_iam_service_linked_role" "elasticloadbalancing" {
  aws_service_name = "elasticloadbalancing.amazonaws.com"
}

resource "aws_iam_service_linked_role" "globalaccelerator" {
  aws_service_name = "globalaccelerator.amazonaws.com"
}
