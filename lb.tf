resource "aws_lb" "stylesage" {
  name               = "stylesage"
  internal           = true
  load_balancer_type = "network"
  subnets            = module.vpc.private_subnets
  tags               = var.tags
}
